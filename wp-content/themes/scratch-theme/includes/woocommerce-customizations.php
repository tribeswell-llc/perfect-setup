<?php

// Reset WooCommerce styles in order to override
add_filter( 'woocommerce_enqueue_styles', 'scratch_reset_styles' );
function scratch_reset_styles( $styles ) {
  return $styles;
}

// Woo Wrapper Start
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
add_action('woocommerce_before_main_content', 'scratch_woo_wrapper_start', 10);
function scratch_woo_wrapper_start() {
  echo '<main><div class="wrap pad clearfix">';
}

// Woo Wrapper End
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
add_action('woocommerce_after_main_content', 'scratch_woo_wrapper_end', 10);
function scratch_woo_wrapper_end() {
  echo '</div></main>';
}

// Declare Theme Support
add_action( 'after_setup_theme', 'scratch_woocommerce_support' );
function scratch_woocommerce_support() {
  add_theme_support( 'woocommerce' );
  add_theme_support( 'wc-product-gallery-zoom' );
  add_theme_support( 'wc-product-gallery-lightbox' );
  add_theme_support( 'wc-product-gallery-slider' );
}

// Remove Product Count
add_filter( 'woocommerce_subcategory_count_html', 'scratch_hide_woo_category_count');
function scratch_hide_woo_category_count() {
  return false;
}

// Remove Product Sorting Form
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

// Remove WooCommerce Sidebar
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

// Remove Breadcrumbs
/*add_action( 'init', 'scratch_remove_wc_breadcrumbs' );
function scratch_remove_wc_breadcrumbs() {
  remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
}*/

// Change breadcrumb delimiter
add_filter( 'woocommerce_breadcrumb_defaults', 'scratch_breadcrumb_delimiter' );
function scratch_breadcrumb_delimiter( $defaults ) {
  // Change the breadcrumb delimeter from ' / ' to ' > '
  $defaults['delimiter'] = ' &gt; ';
  return $defaults;
}

add_filter( 'woocommerce_product_add_to_cart_text' , 'scratch_product_add_to_cart_text' );

function scratch_product_add_to_cart_text() {
  global $product;

  $product_type = $product->get_type();

  switch ( $product_type ) {
    case 'external':
      return __( 'Buy product', 'woocommerce' );
    break;
    case 'grouped':
      return __( 'View Collection', 'woocommerce' );
    break;
    case 'simple':
      return __( 'View Product', 'woocommerce' );
    break;
    case 'variable':
      return __( 'View Options', 'woocommerce' );
    break;
    default:
      return __( 'Read more', 'woocommerce' );
  }

}

// Change number of products per row
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
  function loop_columns() {
    return 4; // Number of columns
  }
}

// Remove Sale! badge
add_filter('woocommerce_sale_flash', 'woo_custom_hide_sales_flash');
function woo_custom_hide_sales_flash() {
  return false;
}

// Remove Reviews tab
add_filter( 'woocommerce_product_tabs', 'woo_remove_reviews_tab', 98);
function woo_remove_reviews_tab($tabs) {
  unset($tabs['reviews']);
  return $tabs;
}

// Replace the placeholder product image
add_action( 'init', 'scratch_product_placeholder_thumbnail' );
function scratch_product_placeholder_thumbnail() {
  add_filter('woocommerce_placeholder_img_src', 'scratch_placeholder_img_src');

  function scratch_placeholder_img_src( $src ) {
    // Find src
    // $upload_dir = wp_upload_dir();
    // $uploads = untrailingslashit( $upload_dir['baseurl'] );
    // $src = $uploads . '/2012/07/thumb1.jpg';
    // and return it
    return $src;
  }
}

?>

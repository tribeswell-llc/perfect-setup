<?php get_header(); the_post(); ?>

<main>

    <article id="post-<?php the_ID(); ?>"
             <?php post_class('wrap'); ?>
             itemscope itemtype="http://schema.org/BlogPosting">

      <header>
        <h1 itemprop="headline">
          <?php the_title(); ?>
        </h1>
        <p class="post-meta"><span class="fa fa-calendar"></span> <time itemprop="datePublished" datetime="<?php echo get_the_date('c'); ?>"><?php echo get_the_date('F j, Y'); ?></time></p>
      </header>

      <div itemprop="articleBody">
        <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'full'); ?>
        <?php the_content(); ?>
      </div>

    </article>

</main>

<?php get_footer(); ?>

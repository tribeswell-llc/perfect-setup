<?php get_header(); the_post(); ?>

<main>

  <section class="wrap">

    <article id="post-<?php the_ID(); ?>"
             <?php post_class(); ?>>

      <header>
        <h1><?php the_title(); ?></h1>
      </header>

      <?php the_content(); ?>

    </article>

  </section>

</main>

<?php get_footer(); ?>

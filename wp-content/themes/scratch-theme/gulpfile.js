const gulp = require('gulp'),
  $ = require('gulp-load-plugins')(),
  merge = require('merge-stream'),
  autoprefixer = require('autoprefixer'),
  mqpacker = require('css-mqpacker'),
  cssnano = require('cssnano'),
  browserSync = require('browser-sync').create();

/**
 * Handle errors and alert the user.
 */
function handleErrors () {
  const args = Array.prototype.slice.call( arguments );

  $.notify.onError( {
    'title': 'Task Failed [<%= error.message %>',
    'message': 'See console.',
    'sound': 'Sosumi' // See: https://github.com/mikaelbr/node-notifier#all-notification-options-with-their-defaults
  } ).apply( this, args );

  $.util.beep(); // Beep 'sosumi' again.

  // Prevent the 'watch' task from stopping.
  this.emit( 'end' );
}

/**
 * Optimize images.
 *
 * https://www.npmjs.com/package/gulp-imagemin
 */
gulp.task( 'imagemin', () =>
  gulp.src( [ '../../uploads/**/*.{png,PNG,jpg,JPG,jpeg,JPEG,gif,GIF}' ], { 'base': '.' } )
    .pipe( $.plumber( { 'errorHandler': handleErrors } ) )
    .pipe( $.newer( '../uploads' ) )
    .pipe($.imagemin([
      $.imagemin.gifsicle({ interlaced: true }),
      $.imagemin.jpegtran({ progressive: true }),
      $.imagemin.optipng({ optimizationLevel: 5 }),
      $.imagemin.svgo({
        plugins: [
          { removeViewBox: true },
          { cleanupIDs: false }
        ]
      })
    ], {
      verbose: true
    }))
    .pipe( gulp.dest( '../uploads' ) )
    .pipe( browserSync.stream() )
);

/**
 * Copy font and any other assets.
 *
 * https://www.npmjs.com/package/merge-stream
 */
gulp.task( 'copy:fonts', () => {
  const css = gulp.src([
      'node_modules/slick-carousel/slick/ajax-loader.gif'
    ])
    .pipe( gulp.dest( 'assets/css' ) );

  const fonts = gulp.src([
      'node_modules/slick-carousel/slick/fonts/*',
      'node_modules/font-awesome/fonts/*'
    ])
    .pipe( gulp.dest( 'assets/fonts' ) );

  merge(css, fonts);
});

/**
 * Concatenate JavaScript.
 *
 * https://www.npmjs.com/package/gulp-sourcemaps
 * https://www.npmjs.com/package/gulp-concat
 */
gulp.task( 'scripts:concat', () =>
  gulp.src([
    'node_modules/magnific-popup/dist/jquery.magnific-popup.js',
    'node_modules/velocity-animate/velocity.js',
    'node_modules/velocity-animate/velocity.ui.js',
    'node_modules/waypoints/lib/jquery.waypoints.js',
    'node_modules/waypoints/lib/shortcuts/infinite.js',
    'node_modules/waypoints/lib/shortcuts/inview.js',
    'node_modules/waypoints/lib/shortcuts/sticky.js',
    'node_modules/slick-carousel/slick/slick.js',
    'assets/js/plugins.js',
    'assets/js/acf-google-maps.js',
    'assets/js/main.js'
  ])
    .pipe( $.plumber( { 'errorHandler': handleErrors } ) )
    .pipe( $.sourcemaps.init() )
    .pipe( $.concat( 'main.js' ) )
    .pipe( $.sourcemaps.write( '.' ) )
    .pipe( gulp.dest( 'assets/js/concat' ) )
);

/**
 * Minify JavaScript.
 *
 * https://www.npmjs.com/package/gulp-uglify
 */
gulp.task( 'scripts:uglify', [ 'scripts:concat' ], () =>
  gulp.src( 'assets/js/concat/main.js' )
    .pipe( $.plumber( { 'errorHandler': handleErrors } ) )
    .pipe( $.sourcemaps.init() )
    .pipe( $.rename( { 'suffix': '.min' } ) )
    .pipe( $.uglify( { 'mangle': false } ) )
    .pipe( $.sourcemaps.write( '.' ) )
    .pipe( gulp.dest( 'assets/js/compiled' ) )
    .pipe( browserSync.stream() )
);

/**
 * Compile Sass and run stylesheet through PostCSS.
 *
 * https://www.npmjs.com/package/gulp-sass
 * https://www.npmjs.com/package/gulp-postcss
 * https://www.npmjs.com/package/gulp-autoprefixer
 * https://www.npmjs.com/package/css-mqpacker
 */
gulp.task( 'styles', () =>
  gulp.src( 'assets/scss/*.scss' )
    .pipe( $.plumber( { 'errorHandler': handleErrors } ) )
    .pipe( $.sourcemaps.init() )
    .pipe( $.sass( {
      'includePaths': [
        'node_modules/animate-sass/',
        'node_modules/include-media/dist/',
        'node_modules/magnific-popup/dist/',
        'node_modules/normalize-scss/sass/',
        'node_modules/font-awesome/scss/',
        'node_modules/slick-carousel/slick/'
      ],
      'errLogToConsole': true,
      'outputStyle': 'expanded'
    } ) )
    .pipe( $.postcss( [
      autoprefixer(),
      mqpacker( { 'sort': true } ),
      cssnano( { 'safe': true } )
    ] ) )
    .pipe( $.rename( { 'suffix': '.min' } ) )
    .pipe( $.sourcemaps.write( '.' ) )
    .pipe( gulp.dest( 'assets/css' ) )
    .pipe( browserSync.stream() )
);

/**
 * Process tasks and reload browsers on file changes.
 *
 * https://www.npmjs.com/package/browser-sync
 */
gulp.task( 'watch', () => {
  browserSync.init({
    'injectChanges': true,
    'open': false,
    'proxy': 'https://perfect-setup.local', // change this to match your host
    'watchOptions': {
      'debounceDelay': 1000 // Wait 1 second before injecting changes
    }
  });

  gulp.watch(['**/*.html', '**/*.php']).on('change', browserSync.reload);
  gulp.watch(['../../uploads/**/*'], ['img']);
  gulp.watch(['assets/js/*.js'], ['js']);
  gulp.watch(['assets/scss/**/*.scss'], ['sass']);
});

/**
 * Create individual tasks.
 */
gulp.task( 'img', [ 'imagemin' ] );
gulp.task( 'js', [ 'scripts:uglify' ] );
gulp.task( 'sass', [ 'styles' ] );
gulp.task( 'default', [ 'watch' ] );

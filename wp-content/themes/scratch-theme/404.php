<?php get_header(); ?>

<main>

  <section class="wrap">

    <article itemscope="" itemtype="http://schema.org/WebPage">

      <h1 itemprop="headline">Page Not Found</h1>

      <p itemprop="mainContentofPage">Sorry, but the page you were trying to view does not exist.</p>

    </article>

  </section>

</main>

<?php get_footer(); ?>

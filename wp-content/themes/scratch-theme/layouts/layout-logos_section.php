<?php global $layout_count; ?>

<section id="scratch-layout-<?php echo $layout_count; ?>-id-<?php the_ID(); ?>"
         class="logos">

  <div class="wrap">

    <?php if (get_sub_field('blurb')) : ?>
    <div class="blurb"><?php the_sub_field( 'blurb' ); ?></div>
    <?php endif; ?>

    <?php $logo_class = "logo-display";

      switch( get_sub_field( 'logo_display' ) ) {

        case 'slider':
          $logo_class .= ' slide-logos';
          break;

        case 'all':
          $logo_class .= ' all-logos';
          break;

      }
    ?>

    <?php if ( have_rows( 'logos' ) ) : ?>

      <div class="<?php echo $logo_class; ?>">

        <?php while ( have_rows( 'logos' ) ) : the_row(); ?>

        <div class="logo">

          <?php if ( get_sub_field( 'link' ) ) : ?>
            <a target="_blank" href="<?php the_sub_field( 'link' ); ?>">
          <?php endif; ?>

          <?php echo wp_get_attachment_image( get_sub_field( 'logo' ), 'thumbnail' ); ?>

          <?php if ( get_sub_field( 'link' ) ) : ?>
            </a>
          <?php endif; ?>

        </div>

        <?php endwhile; ?>

      </div>

    <?php endif; ?>

  </div>

</section>

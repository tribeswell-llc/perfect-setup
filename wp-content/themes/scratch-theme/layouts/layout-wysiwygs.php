<?php global $layout_count; ?>

<section id="scratch-layout-<?php echo $layout_count; ?>-id-<?php the_ID(); ?>"
         class="wysiwygs">

  <div class="flex-wrap">

    <?php if (get_sub_field('blurb')) : ?>
    <div class="blurb"><?php the_sub_field( 'blurb' ); ?></div>
    <?php endif; ?>

    <?php
      $wysiwygs = get_sub_field( 'wysiwygs' );

      if ( get_sub_field( 'offset' ) !== 'Flexible' ) {

        if ( get_sub_field( 'offset' ) === '2 to 1' ) {

          $offset = '2:1';

        } else {

          $offset = '1:2';

        }

      } else {

        $offset = null;

      }

      if ( have_rows( 'wysiwygs' ) ) {

        scratch_layout_declare( $wysiwygs, 2, true, $offset );

        while( have_rows( 'wysiwygs' ) ) { the_row();

          scratch_layout_start();

            the_sub_field( 'wysiwyg' );

          scratch_layout_end();

        }

      }
    ?>

  </div>

</section>

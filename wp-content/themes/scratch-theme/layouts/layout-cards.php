<?php global $layout_count; ?>

<section id="scratch-layout-<?php echo $layout_count; ?>-id-<?php the_ID(); ?>"
         class="cards">

  <div class="flex-wrap">

    <?php if (get_sub_field('blurb')) : ?>
    <div class="blurb"><?php the_sub_field( 'blurb' ); ?></div>
    <?php endif; ?>

    <?php
      $columns = (int) get_sub_field( 'number_of_columns' );

      if ( get_sub_field( 'flexible_columns' ) !== null ) {
        $flex = get_sub_field( 'flexible_columns' );
      } else {
        $flex = true;
      }

      $offset = null;

      if ( $columns === 2 ) {
        if ( get_sub_field( '2_col_offset' ) !== 'Equal' ) {
          if ( get_sub_field( '2_col_offset' ) === '2 to 1' ) {
            $offset = '2:1';
          } else {
            $offset = '1:2';
          }
        }
      }

      if ( $columns === 3 ) {
        if ( get_sub_field( '3_col_offset' ) !== 'Equal' ) {
          if ( get_sub_field( '3_col_offset' ) === '1 to 1 to 2' ) {
            $offset = '1:1:2';
          } elseif ( get_sub_field( '3_col_offset' ) === '1 to 2 to 1' ) {
            $offset = '1:2:1';
          } else {
            $offset = '2:1:1';
          }
        }
      }

    ?>

    <?php if ( get_sub_field( 'content_choice' ) === 'custom' ) :

      $cards = get_sub_field( 'cards' );

      if ( $cards ) :

        scratch_layout_declare( $cards, $columns, $flex, $offset );

        while ( have_rows( 'cards' ) ) : the_row();

         scratch_layout_start(); ?>

        <?php if ( get_sub_field( 'image' ) ) : ?>

          <?php echo wp_get_attachment_image( get_sub_field( 'image' ), 'medium', '', array( 'alt' => get_sub_field( 'header' ), 'class' => 'card-img-top' ) ); ?>

        <?php endif; ?>

        <div class="card-block">

          <?php if ( get_sub_field( 'header' ) ) : ?>
            <h4 class="card-title"><?php the_sub_field( 'header' ); ?></h4>
          <?php endif; ?>

          <div class="card-text">

            <?php the_sub_field( 'blurb' ); ?>

          </div>

          <?php if ( get_sub_field( 'add_button' ) ) : $button = get_sub_field('button_link'); ?>

            <a class="button"
               target="<?php echo $button['target']; ?>"
               href="<?php echo $button['url']; ?>"
               title="<?php echo $button['title']; ?>">
              <?php echo $button['title']; ?>
            </a>

          <?php endif; ?>

        </div>

    <?php
          scratch_layout_end();
        endwhile;
      endif;

    elseif ( get_sub_field( 'content_choice' ) === 'posts' ) :

      $cards = get_sub_field( 'featured_posts' );

      if ( $cards ) :

        scratch_layout_declare( $cards, $columns, $flex, $offset );

        foreach ($cards as $post) : setup_postdata($post);

         scratch_layout_start(); ?>

        <?php if ( has_post_thumbnail() ) : ?>

          <?php echo wp_get_attachment_image( get_post_thumbnail_id(), 'medium', '', array( 'alt' => get_the_title(), 'class' => 'card-img-top' ) ); ?>

        <?php endif; ?>

        <div class="card-block" itemscope itemtype="http://schema.org/BlogPosting">

          <h4 class="card-title" itemprop="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

          <h6 class="card-subtitle"><time itemprop="datePublished" datetime="<?php echo get_the_date('c'); ?>"><?php echo get_the_date(' F j, Y'); ?></time></h6>

          <div class="card-text" itemprop="articleBody">

            <?php the_excerpt(); ?>

          </div>

        </div>

    <?php
          scratch_layout_end(); wp_reset_postdata();
        endforeach;
      endif;
    endif;
    ?>
  </div>
</section>

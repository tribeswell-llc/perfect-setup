<?php global $layout_count; ?>

<section id="scratch-layout-<?php echo $layout_count; ?>-id-<?php the_ID(); ?>"
         class="flexible-columns">

  <div class="flex-wrap">

    <?php if (get_sub_field('blurb')) : ?>
    <div class="blurb"><?php the_sub_field( 'blurb' ); ?></div>
    <?php endif; ?>

    <?php
      $columns = get_sub_field( 'columns' );

      $column_number = (int) get_sub_field( 'number_of_columns' );

      if ( get_sub_field( 'flexible_columns' ) !== null ) {
        $column_flex = get_sub_field( 'flexible_columns' );
      } else {
        $column_flex = true;
      }

      $column_offset = null;

      if ( $column_number === 2 ) {
        if ( get_sub_field( '2_col_offset' ) !== 'Equal' ) {
          if ( get_sub_field( '2_col_offset' ) === '2 to 1' ) {
            $column_offset = '2:1';
          } else {
            $column_offset = '1:2';
          }
        }
      }

      if ( $column_number === 3 ) {
        if ( get_sub_field( '3_col_offset' ) !== 'Equal' ) {
          if ( get_sub_field( '3_col_offset' ) === '1 to 1 to 2' ) {
            $column_offset = '1:1:2';
          } elseif ( get_sub_field( '3_col_offset' ) === '1 to 2 to 1' ) {
            $column_offset = '1:2:1';
          } else {
            $column_offset = '2:1:1';
          }
        }
      }

      if ( have_rows( 'columns' ) ) :

        scratch_layout_declare( $columns, $column_number, $column_flex, $column_offset );

        while ( have_rows( 'columns' ) ) : the_row();

          scratch_layout_start();
    ?>

      <?php if ( get_sub_field( 'icon_or_image' ) ) : ?>

        <?php if ( get_sub_field( 'image' ) ) : ?>
          <?php if (get_sub_field('circle_image')) : ?>
          <div class="scratch-bg circle"
               style="background-image: url('<?php echo wp_get_attachment_image_url( get_sub_field( 'image' ), 'thumbnail' ); ?>');">
          </div>
          <?php else : ?>
            <?php echo wp_get_attachment_image( get_sub_field( 'image' ), 'thumbnail' ); ?>
          <?php endif; ?>
        <?php endif; ?>

      <?php else: ?>

        <?php if ( get_sub_field( 'icon' ) ) : ?>
          <div class="circle center">
            <i class="fa <?php the_sub_field( 'icon' ); ?> hvalign"></i>
          </div>
        <?php endif; ?>

      <?php endif; ?>

      <?php if ( get_sub_field( 'header' ) ) : ?>
        <h3>
          <?php if ( get_sub_field( 'add_button' ) ) : $button = get_sub_field('button_link'); ?>
            <a target="<?php echo $button['target']; ?>"
               href="<?php echo $button['url']; ?>"
               title="<?php echo $button['title']; ?>">
          <?php endif; ?>

          <?php the_sub_field( 'header' ); ?>

          <?php if ( get_sub_field( 'add_button' ) ) : ?>
            </a>
          <?php endif; ?>
        </h3>
      <?php endif; ?>

      <?php the_sub_field( 'blurb' ); ?>

      <?php if ( get_sub_field( 'add_button' ) ) : $button = get_sub_field('button_link'); ?>

        <a class="button"
           target="<?php echo $button['target']; ?>"
           href="<?php echo $button['url']; ?>"
           title="<?php echo $button['title']; ?>">
          <?php echo $button['title']; ?>
        </a>

      <?php endif; ?>

    <?php
        scratch_layout_end();
      endwhile;
    endif;
    ?>
  </div>
</section>

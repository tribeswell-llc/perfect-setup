<?php global $layout_count; ?>

<section id="scratch-layout-<?php echo $layout_count; ?>-id-<?php the_ID(); ?>"
         class="hero-unit">

  <div class="scratch-bg"
       <?php if ( get_sub_field( 'background_image' ) ) { ?>style="background-image: url('<?php echo wp_get_attachment_image_url( get_sub_field( 'background_image' ), 'full' ); ?>'); background-position: <?php echo get_sub_field( 'bg_image_pos_x' ) . ' ' . get_sub_field( 'bg_image_pos_y' ); ?>;"<?php } ?>>

    <div class="overlay clearfix">

      <div class="flex-wrap">

        <div class="content blurb white"
             style="padding: <?php the_sub_field( 'text_margin' ); ?>rem 0;">

          <?php the_sub_field( 'blurb' ); ?>

          <?php if ( get_sub_field( 'add_button' ) ) : $button = get_sub_field('button_link'); ?>

            <a class="button"
               target="<?php echo $button['target']; ?>"
               href="<?php echo $button['url']; ?>"
               title="<?php echo $button['title']; ?>">
              <?php echo $button['title']; ?>
            </a>

          <?php endif; ?>

        </div>

      </div>

    </div>

  </div>

</section>

<?php global $layout_count; ?>

<section id="scratch-layout-<?php echo $layout_count; ?>-id-<?php the_ID(); ?>"
         class="video">

  <?php if ( get_sub_field( 'video_fallback_image' ) ) : ?>

    <div class="scratch-video" style="background-image: url('<?php echo wp_get_attachment_image_url( get_sub_field('video_fallback_image') ); ?>');"></div>

  <?php endif; ?>

  <video class="scratch-video"
         <?php if( get_sub_field('video_attributes') ) { $attrs = get_sub_field('video_attributes'); foreach($attrs as $attr) { echo $attr . ' '; } } ?>
         poster="<?php echo wp_get_attachment_image_url( get_sub_field('video_fallback_image') ); ?>"
         style="background-image: url('<?php echo wp_get_attachment_image_url( get_sub_field('video_fallback_image') ); ?>');">
    <source src="<?php the_sub_field('video_webm_file'); ?>" type="video/webm">
    <source src="<?php the_sub_field('video_mp4_file'); ?>" type="video/mp4">
    <source src="<?php the_sub_field('video_ogv_file'); ?>" type="video/ogg">
    <!--fallback-->
    <img src="<?php echo wp_get_attachment_image_url( get_sub_field('video_fallback_image') ); ?>" alt="Image being shown because HTML5 video is not supported." />
  </video>

  <div class="overlay clearfix">
    <div class="wrap white">
      <div class="hvalign-always center">

        <div class="blurb"><?php the_sub_field('blurb'); ?></div>

        <?php if ( get_sub_field( 'add_button' ) ) : $button = get_sub_field('button_link'); ?>

          <a class="button"
             target="<?php echo $button['target']; ?>"
             href="<?php echo $button['url']; ?>"
             title="<?php echo $button['title']; ?>">
            <?php echo $button['title']; ?>
          </a>

        <?php endif; ?>
      </div>
    </div>
  </div>

</section>

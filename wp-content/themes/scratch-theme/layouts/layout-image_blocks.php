<?php global $layout_count; ?>

<section id="scratch-layout-<?php echo $layout_count; ?>-id-<?php the_ID(); ?>"
         class="image-blocks">

    <?php
      if ( get_sub_field( 'offset' ) !== 'Flexible' ) {

        if ( get_sub_field( 'offset' ) === '2 to 1' ) {

          $offset = '2:1';

        } else {

          $offset = '1:2';

        }

      } else {

        $offset = null;

      }
    ?>

    <?php if ( get_sub_field( 'content_choice' ) === 'custom' ) :

      $blocks = get_sub_field( 'image_blocks' );


      if ( $blocks ) :

        scratch_layout_declare( $blocks, 2, true, $offset );

        while ( have_rows( 'image_blocks' ) ) : the_row();

         scratch_layout_start(); ?>

          <div class="tile"
            <?php if ( get_sub_field( 'background_image' ) ) { ?>
            style="background-image: url('<?php echo wp_get_attachment_url( get_sub_field( 'background_image' ), 'thumbnail' ); ?>');"
            <?php } ?>>

            <?php if (get_sub_field('blurb')) : ?>
            <div class="overlay"></div>
            <?php endif; ?>

            <div class="<?php if ( get_sub_field( 'animate' ) ) { echo 'animated '; } ?>post-content blurb" <?php if ( get_sub_field( 'animate' ) && get_sub_field( 'animation' ) ) { echo 'data-animate="' . get_sub_field( 'animation' ) . '"'; } ?>>

              <?php the_sub_field('blurb'); ?>

              <?php if ( get_sub_field( 'add_button' ) ) : $button = get_sub_field('button_link'); ?>

                <a class="button"
                   target="<?php echo $button['target']; ?>"
                   href="<?php echo $button['url']; ?>"
                   title="<?php echo $button['title']; ?>">
                  <?php echo $button['title']; ?>
                </a>

              <?php endif; ?>

            </div>

          </div>

    <?php
        scratch_layout_end();
      endwhile;
    endif;

    elseif (get_sub_field('content_choice') === 'posts') :

      $featured_posts = get_sub_field('featured_posts');

      scratch_layout_declare($featured_posts, 2, true, $offset);

      foreach ($featured_posts as $post) : setup_postdata($post);

        scratch_layout_start();
    ?>
          <div class="tile"
            <?php if ( has_post_thumbnail() ) { ?>
            style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ), 'thumbnail' ); ?>');"
            <?php } ?>>

            <div class="overlay"></div>

            <div class="post-content blurb" <?php if ( get_sub_field( 'post_animate' ) ) { echo 'animated'; } ?>" <?php if ( get_sub_field( 'post_animate' ) && get_sub_field( 'post_animation' ) ) { echo 'data-animate="' . get_sub_field( 'post_animation' ) . '"'; } ?>>

              <h3 class="layout-header"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>

              <?php the_excerpt(); ?>

            </div>

          </div>
    <?php
        scratch_layout_end();
      endforeach; wp_reset_postdata();
    endif;
    ?>

</section>

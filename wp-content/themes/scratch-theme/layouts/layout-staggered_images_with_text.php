<?php global $layout_count; ?>

<section id="scratch-layout-<?php echo $layout_count; ?>-id-<?php the_ID(); ?>"
         class="staggered">

  <?php if ( have_rows( 'rows' ) ) : $row_count = 1; ?>

    <?php while ( have_rows( 'rows' ) ) : the_row(); ?>

      <div class="row">

        <div class="flex-wrap">

          <div class="fivecol <?php echo ($row_count % 2 === 0) ? 'last' : 'first'; ?>">

            <?php if ( get_sub_field( 'icon_or_image' ) ) : ?>

              <?php if ( get_sub_field( 'image' ) ) : ?>
                <?php if (get_sub_field('circle_image')) : ?>
                <div class="scratch-bg circle"
                    style="background-image: url('<?php echo wp_get_attachment_image_url( get_sub_field( 'image' ), 'thumbnail' ); ?>');">
                </div>
                <?php else : ?>
                  <?php echo wp_get_attachment_image( get_sub_field( 'image' ), 'thumbnail' ); ?>
                <?php endif; ?>
              <?php endif; ?>

            <?php else: ?>

              <?php if ( get_sub_field( 'icon' ) ) : ?>
                <div class="circle icon">
                  <i class="fa <?php the_sub_field( 'icon' ); ?>"></i>
                </div>
              <?php endif; ?>

            <?php endif; ?>
          </div>

          <div class="sevencol <?php echo ($row_count % 2 === 0) ? 'first' : 'last'; ?>">
            <div class="blurb center">

              <?php the_sub_field( 'blurb' ); ?>

              <?php if ( get_sub_field( 'add_button' ) ) : $button = get_sub_field('button_link'); ?>

                <a class="button"
                   target="<?php echo $button['target']; ?>"
                   href="<?php echo $button['url']; ?>"
                   title="<?php echo $button['title']; ?>">
                  <?php echo $button['title']; ?>
                </a>

              <?php endif; ?>
            </div>
          </div>

        </div>
      </div>
    <?php $row_count++; endwhile; ?>
  <?php endif; ?>
</section>

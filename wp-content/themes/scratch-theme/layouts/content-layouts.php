<?php

if (have_rows('layouts')) { $GLOBALS['layout_count'] = 1;

  while (have_rows('layouts')) { the_row();

    get_template_part('layouts/layout', get_row_layout());

    $GLOBALS['layout_count']++;

  }

} else { ?>

  <p class="center">You haven't added any layouts yet. <?php edit_post_link('Add one now.'); ?></p>

<?php
}

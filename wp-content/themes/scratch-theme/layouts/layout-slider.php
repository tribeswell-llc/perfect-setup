<?php global $layout_count; ?>

<section id="scratch-layout-<?php echo $layout_count; ?>-id-<?php the_ID(); ?>"
         class="slider-row">

  <?php if ( have_rows( 'slides' ) ) : ?>

    <div class="slick">

      <?php while ( have_rows( 'slides' ) ) : the_row(); ?>

        <div class="slide"
             style="background-image: url('<?php the_sub_field( 'background' ); ?>');">

          <div class="overlay clearfix">

            <div class="wrap hvalign-always">

              <div class="slide-text blurb center">

                <?php the_sub_field( 'blurb' ); ?>

                <?php if ( get_sub_field( 'add_button' ) ) : $button = get_sub_field('button_link'); ?>

                  <a class="button"
                     target="<?php echo $button['target']; ?>"
                     href="<?php echo $button['url']; ?>"
                     title="<?php echo $button['title']; ?>">
                    <?php echo $button['title']; ?>
                  </a>

                <?php endif; ?>

              </div>

            </div>

          </div>

        </div>

      <?php endwhile; ?>

    </div>

  <?php endif; ?>

</section>

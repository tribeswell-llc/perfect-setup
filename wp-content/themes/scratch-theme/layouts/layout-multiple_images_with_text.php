<?php global $layout_count; ?>

<section id="scratch-layout-<?php echo $layout_count; ?>-id-<?php the_ID(); ?>"
         class="multiple">

  <div class="flex-wrap">

    <div class="images sixcol <?php echo get_sub_field( 'image_side' ) ? 'last' : 'first'; ?>">

      <?php if ( get_sub_field( 'images' ) ) :

        switch ( count( get_sub_field( 'images' ) ) ) {
          case 1:
            $image_class = 'full';
            break;

          case 2:
            $image_class = 'half';
            break;

          case 3:
            $image_class = 'third';
            break;

          case 4:
            $image_class = 'quarter';
            break;
        }
      ?>

      <div class="gallery">
        <?php foreach ( get_sub_field( 'images' ) as $image ) {

            echo wp_get_attachment_image(
              $image[ 'ID' ],
              'medium',
              false,
              array (
                'class' => $image_class
              )
            );
        }
        ?>
      </div>

      <?php endif; ?>

    </div>

    <div class="content sixcol <?php echo get_sub_field( 'image_side' ) ? 'first' : 'last'; ?>">

        <?php the_sub_field( 'blurb' ); ?>

        <?php if ( get_sub_field( 'add_button' ) ) : $button = get_sub_field('button_link'); ?>

          <a class="button"
             target="<?php echo $button['target']; ?>"
             href="<?php echo $button['url']; ?>"
             title="<?php echo $button['title']; ?>">
            <?php echo $button['title']; ?>
          </a>

        <?php endif; ?>
    </div>

  </div>

</section>

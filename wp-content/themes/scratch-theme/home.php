<?php get_header(); ?>

<main>

  <?php if (have_posts()): ?>
    <?php while (have_posts()): the_post(); ?>

    <article id="post-<?php the_ID(); ?>"
             <?php post_class('flex-wrap'); ?>
             itemscope itemtype="http://schema.org/BlogPosting">

      <?php if (has_post_thumbnail()) : ?>
        <div class="fourcol first">
          <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'medium'); ?>
        </div>
        <div class="eightcol last">
      <?php endif; ?>

      <header>
        <h2 itemprop="headline">
          <a href="<?php the_permalink(); ?>"
             title="<?php the_title_attribute(); ?>">
            <?php the_title(); ?>
          </a>
        </h2>
        <p class="post-meta"><span class="fa fa-calendar"></span> <time itemprop="datePublished" datetime="<?php echo get_the_date('c'); ?>"><?php echo get_the_date('F j, Y'); ?></time></p>
      </header>

      <div itemprop="articleBody">
        <?php the_excerpt(); ?>
      </div>

      <?php if (has_post_thumbnail()) : ?>
        </div>
      <?php endif; ?>

    </article>

    <?php endwhile; ?>

    <?php echo get_the_posts_pagination(array(
        'mid_size' => 2,
        'prev_text' => '<i class="fa fa-chevron-left"></i> Previous',
        'next_text' => 'Next <i class="fa fa-chevron-right"></i>',
    )); ?>

  <?php else: ?>

      <p>No posts here.</p>

  <?php endif; ?>

</main>

<?php get_footer(); ?>

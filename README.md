# Scratch Theme for WordPress + ACF

## Includes WordPress, Plugins, and Scratch Theme

When Local (and WPEngine) creates a new site for you, it installs the latest WordPress installation and latest default theme like Twentyeighteen. Since Scratch Theme is dependent on plugins such as ACF Pro, we created a repo that houses WordPress, the usual plugins we use, and Scratch Theme altogether. So the first thing we need to do is remove Local's install and replace it with our Perfect Setup repo.

## Clone Perfect Setup

Once you're site's created you'll be able to see all the details about it including where it's located. Open your terminal and `cd` to match your Site Path.

The most important file in any WordPress installation is the `wp-config.php` file so we need to make sure we don't remove that. So the first thing we'll need to do is move that file into safety with `mv app/public/wp-config.php app/`. You can make sure it was successful with `ls app/`.

Now we can safely remove and replace Local's installation with our Perfect Setup. Move into the `app` directory with `cd app` and remove the `public` folder with `rm -r public/`. Now we can replace it with `git clone git@bitbucket.org:tribeswell-llc/perfect-setup.git public`. This clones our Perfect Setup repo and renames the folder to `public` to keep everything consistent and working.

## Install npm Packages

The last thing we need to complete installation is to install Scratch's npm packages. We don't include these in the repo because it will make it too large. Move into the theme directory with `cd public/wp-content/themes/scratch-theme/` and `npm install` to install the packages.
